export type String = string
export type Boolean = boolean
export type Int = number
export type BigInt = number
export type Float = number
export type Currency = string
