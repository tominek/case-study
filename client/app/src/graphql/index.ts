import gql from 'graphql-tag'
import * as VueApolloComposable from '@vue/apollo-composable'
import * as VueCompositionApi from 'vue'

export type Maybe<T> = T;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
export type ReactiveFunction<TParam> = () => TParam;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  BigInt: number;
  Currency: string;
};


export type IConversionInput = {
  amount: Scalars['Float'];
  baseCurrency: Scalars['Currency'];
  targetCurrency: Scalars['Currency'];
};

export type IConversionResult = {
  __typename: 'ConversionResult';
  amount: Scalars['Float'];
  baseCurrency: Scalars['Currency'];
  targetCurrency: Scalars['Currency'];
  convertedAmount: Scalars['Float'];
};


export type IMutation = {
  __typename: 'Mutation';
  convert: IConversionResult;
};


export type IMutationConvertArgs = {
  input: IConversionInput;
};

export type IQuery = {
  __typename: 'Query';
  supportedCurrencies: Array<Maybe<Scalars['Currency']>>;
  mostPopularCurrency?: Maybe<Scalars['Currency']>;
  totalConvertedInUSD: Scalars['Float'];
  conversionsCount: Scalars['BigInt'];
};

export type IConvertMutationVariables = Exact<{
  input: IConversionInput;
}>;


export type IConvertMutation = (
  { __typename: 'Mutation' }
  & {
  convert: (
    { __typename: 'ConversionResult' }
    & Pick<IConversionResult, 'amount' | 'baseCurrency' | 'targetCurrency' | 'convertedAmount'>
    )
}
  );

export type IConversionsCountQueryVariables = Exact<{ [key: string]: never; }>;


export type IConversionsCountQuery = (
  { __typename: 'Query' }
  & Pick<IQuery, 'conversionsCount'>
  );

export type IMostPopularCurrencyQueryVariables = Exact<{ [key: string]: never; }>;


export type IMostPopularCurrencyQuery = (
  { __typename: 'Query' }
  & Pick<IQuery, 'mostPopularCurrency'>
  );

export type IStatisticsQueryVariables = Exact<{ [key: string]: never; }>;


export type IStatisticsQuery = (
  { __typename: 'Query' }
  & Pick<IQuery, 'conversionsCount' | 'mostPopularCurrency' | 'totalConvertedInUSD'>
  );

export type ISupportedCurrenciesQueryVariables = Exact<{ [key: string]: never; }>;


export type ISupportedCurrenciesQuery = (
  { __typename: 'Query' }
  & Pick<IQuery, 'supportedCurrencies'>
  );

export type ITotalConvertedInUsdQueryVariables = Exact<{ [key: string]: never; }>;


export type ITotalConvertedInUsdQuery = (
  { __typename: 'Query' }
  & Pick<IQuery, 'totalConvertedInUSD'>
  );


export const ConvertDocument = gql`
  mutation convert($input: ConversionInput!) {
    convert(input: $input) {
      amount
      baseCurrency
      targetCurrency
      convertedAmount
    }
  }
`

/**
 * __useConvertMutation__
 *
 * To run a mutation, you first call `useConvertMutation` within a Vue component and pass it any options that fit your needs.
 * When your component renders, `useConvertMutation` returns an object that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - Several other properties: https://v4.apollo.vuejs.org/api/use-mutation.html#return
 *
 * @param options that will be passed into the mutation, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/mutation.html#options;
 *
 * @example
 * const { mutate, loading, error, onDone } = useConvertMutation({
 *   variables: {
 *     input: // value for 'input'
 *   },
 * });
 */
export function useConvertMutation (options: VueApolloComposable.UseMutationOptions<IConvertMutation, IConvertMutationVariables> | ReactiveFunction<VueApolloComposable.UseMutationOptions<IConvertMutation, IConvertMutationVariables>>) {
  return VueApolloComposable.useMutation<IConvertMutation, IConvertMutationVariables>(ConvertDocument, options)
}

export type ConvertMutationCompositionFunctionResult = VueApolloComposable.UseMutationReturn<IConvertMutation, IConvertMutationVariables>;
export const ConversionsCountDocument = gql`
  query conversionsCount {
    conversionsCount
  }
`

/**
 * __useConversionsCountQuery__
 *
 * To run a query within a Vue component, call `useConversionsCountQuery` and pass it any options that fit your needs.
 * When your component renders, `useConversionsCountQuery` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param options that will be passed into the query, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/query.html#options;
 *
 * @example
 * const { result, loading, error } = useConversionsCountQuery();
 */
export function useConversionsCountQuery (options: VueApolloComposable.UseQueryOptions<IConversionsCountQuery, IConversionsCountQueryVariables> | VueCompositionApi.Ref<VueApolloComposable.UseQueryOptions<IConversionsCountQuery, IConversionsCountQueryVariables>> | ReactiveFunction<VueApolloComposable.UseQueryOptions<IConversionsCountQuery, IConversionsCountQueryVariables>> = {}) {
  return VueApolloComposable.useQuery<IConversionsCountQuery, IConversionsCountQueryVariables>(ConversionsCountDocument, {}, options)
}

export type ConversionsCountQueryCompositionFunctionResult = VueApolloComposable.UseQueryReturn<IConversionsCountQuery, IConversionsCountQueryVariables>;
export const MostPopularCurrencyDocument = gql`
  query mostPopularCurrency {
    mostPopularCurrency
  }
`

/**
 * __useMostPopularCurrencyQuery__
 *
 * To run a query within a Vue component, call `useMostPopularCurrencyQuery` and pass it any options that fit your needs.
 * When your component renders, `useMostPopularCurrencyQuery` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param options that will be passed into the query, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/query.html#options;
 *
 * @example
 * const { result, loading, error } = useMostPopularCurrencyQuery();
 */
export function useMostPopularCurrencyQuery (options: VueApolloComposable.UseQueryOptions<IMostPopularCurrencyQuery, IMostPopularCurrencyQueryVariables> | VueCompositionApi.Ref<VueApolloComposable.UseQueryOptions<IMostPopularCurrencyQuery, IMostPopularCurrencyQueryVariables>> | ReactiveFunction<VueApolloComposable.UseQueryOptions<IMostPopularCurrencyQuery, IMostPopularCurrencyQueryVariables>> = {}) {
  return VueApolloComposable.useQuery<IMostPopularCurrencyQuery, IMostPopularCurrencyQueryVariables>(MostPopularCurrencyDocument, {}, options)
}

export type MostPopularCurrencyQueryCompositionFunctionResult = VueApolloComposable.UseQueryReturn<IMostPopularCurrencyQuery, IMostPopularCurrencyQueryVariables>;
export const StatisticsDocument = gql`
  query statistics {
    conversionsCount
    mostPopularCurrency
    totalConvertedInUSD
  }
`

/**
 * __useStatisticsQuery__
 *
 * To run a query within a Vue component, call `useStatisticsQuery` and pass it any options that fit your needs.
 * When your component renders, `useStatisticsQuery` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param options that will be passed into the query, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/query.html#options;
 *
 * @example
 * const { result, loading, error } = useStatisticsQuery();
 */
export function useStatisticsQuery (options: VueApolloComposable.UseQueryOptions<IStatisticsQuery, IStatisticsQueryVariables> | VueCompositionApi.Ref<VueApolloComposable.UseQueryOptions<IStatisticsQuery, IStatisticsQueryVariables>> | ReactiveFunction<VueApolloComposable.UseQueryOptions<IStatisticsQuery, IStatisticsQueryVariables>> = {}) {
  return VueApolloComposable.useQuery<IStatisticsQuery, IStatisticsQueryVariables>(StatisticsDocument, {}, options)
}

export type StatisticsQueryCompositionFunctionResult = VueApolloComposable.UseQueryReturn<IStatisticsQuery, IStatisticsQueryVariables>;
export const SupportedCurrenciesDocument = gql`
  query supportedCurrencies {
    supportedCurrencies
  }
`

/**
 * __useSupportedCurrenciesQuery__
 *
 * To run a query within a Vue component, call `useSupportedCurrenciesQuery` and pass it any options that fit your needs.
 * When your component renders, `useSupportedCurrenciesQuery` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param options that will be passed into the query, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/query.html#options;
 *
 * @example
 * const { result, loading, error } = useSupportedCurrenciesQuery();
 */
export function useSupportedCurrenciesQuery (options: VueApolloComposable.UseQueryOptions<ISupportedCurrenciesQuery, ISupportedCurrenciesQueryVariables> | VueCompositionApi.Ref<VueApolloComposable.UseQueryOptions<ISupportedCurrenciesQuery, ISupportedCurrenciesQueryVariables>> | ReactiveFunction<VueApolloComposable.UseQueryOptions<ISupportedCurrenciesQuery, ISupportedCurrenciesQueryVariables>> = {}) {
  return VueApolloComposable.useQuery<ISupportedCurrenciesQuery, ISupportedCurrenciesQueryVariables>(SupportedCurrenciesDocument, {}, options)
}

export type SupportedCurrenciesQueryCompositionFunctionResult = VueApolloComposable.UseQueryReturn<ISupportedCurrenciesQuery, ISupportedCurrenciesQueryVariables>;
export const TotalConvertedInUsdDocument = gql`
  query totalConvertedInUSD {
    totalConvertedInUSD
  }
`

/**
 * __useTotalConvertedInUsdQuery__
 *
 * To run a query within a Vue component, call `useTotalConvertedInUsdQuery` and pass it any options that fit your needs.
 * When your component renders, `useTotalConvertedInUsdQuery` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param options that will be passed into the query, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/query.html#options;
 *
 * @example
 * const { result, loading, error } = useTotalConvertedInUsdQuery();
 */
export function useTotalConvertedInUsdQuery (options: VueApolloComposable.UseQueryOptions<ITotalConvertedInUsdQuery, ITotalConvertedInUsdQueryVariables> | VueCompositionApi.Ref<VueApolloComposable.UseQueryOptions<ITotalConvertedInUsdQuery, ITotalConvertedInUsdQueryVariables>> | ReactiveFunction<VueApolloComposable.UseQueryOptions<ITotalConvertedInUsdQuery, ITotalConvertedInUsdQueryVariables>> = {}) {
  return VueApolloComposable.useQuery<ITotalConvertedInUsdQuery, ITotalConvertedInUsdQueryVariables>(TotalConvertedInUsdDocument, {}, options)
}

export type TotalConvertedInUsdQueryCompositionFunctionResult = VueApolloComposable.UseQueryReturn<ITotalConvertedInUsdQuery, ITotalConvertedInUsdQueryVariables>;
