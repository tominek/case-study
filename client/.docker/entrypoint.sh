#!/usr/bin/env sh
set -e pipefail

env

npm run build
npm run start

exec "$@"
