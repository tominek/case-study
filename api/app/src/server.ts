import { ApolloServer } from 'apollo-server-express'

import createContext from './modules/context'
import schema from './modules/schema'
import { notInProduction } from './helpers/general'

export default new ApolloServer({
  schema,
  tracing: notInProduction(),
  context: async () => await createContext(),
  playground: notInProduction(),
  introspection: notInProduction(),
})
