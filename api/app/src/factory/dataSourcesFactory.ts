import { CacheDataSource } from '../modules/cache/cache.dataSource'
import { ExchangeRatesDataSource } from '../modules/exchangeRates/exchangeRates.dataSource'

export function getDataSources(): IDataSourcesContainer {
  return {
    Cache: new CacheDataSource(),
    ExchangeRates: new ExchangeRatesDataSource(),
  }
}

export interface IDataSourcesContainer {
  Cache: CacheDataSource,
  ExchangeRates: ExchangeRatesDataSource,
}
