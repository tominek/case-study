import { gql } from 'apollo-server'
import { DocumentNode } from 'graphql'

const types: DocumentNode = gql`
    scalar Int
    scalar String
    scalar BigInt
    scalar Float
    scalar Currency
`

export default types
