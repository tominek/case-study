import { IContextContainer } from './context'
import conversion from './conversion/conversion.resolvers'

export interface IModuleResolvers {
  queries: Record<string, (parent: any, args: any, context: IContextContainer) => any>
  mutations: Record<string, (parent: any, args: any, context: IContextContainer) => any>
  resolvers: Record<string, Record<string, (parent: any, args: any, context: IContextContainer) => any>>
}

const modules: IModuleResolvers[] = [
  conversion,
]

export default function loadResolvers (): Record<string, any> {
  const {
    mutations,
    queries,
    resolvers,
  } = modules.reduce<IModuleResolvers>((acc: IModuleResolvers, m: IModuleResolvers) => {
    acc.mutations = { ...acc.mutations, ...m.mutations }
    acc.queries = { ...acc.queries, ...m.queries }
    acc.resolvers = { ...acc.resolvers, ...m.resolvers }

    return acc
  }, { mutations: {}, queries: {}, resolvers: {} })

  return {
    ...require('../scalars'),
    ...resolvers,
    Query: {
      ...queries,
    },
    Mutation: {
      ...mutations,
    },
  }
}
