import { IContextContainer } from '../context'
import { IExchangeResponse } from '../exchangeRates/exchangeRates.dataSource'

export const getConversionRates = async (
  base: string, {
    Cache,
    ExchangeRates,
  }: IContextContainer,
): Promise<IExchangeResponse> => {
  let conversionRates = await Cache.getConversionRates(base)
  if (!conversionRates) {
    conversionRates = await ExchangeRates.getConversionRates(base)
    await Cache.setConversionRate(base, conversionRates)
  }

  return conversionRates
}

export const incrementConvertedAmount = async (
  base: string,
  amount: number,
  target: string,
  context: IContextContainer
): Promise<void> => {
  const exchangeRates = await getConversionRates(base, context)
  const usdRate = exchangeRates.rates['USD']

  const { Statistics } = context
  await Statistics.incrementConvertedAmount(usdRate * amount)
}
