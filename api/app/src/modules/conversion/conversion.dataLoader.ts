import SuperDataLoader, { createLoader, getCollection } from '../../helpers/dataLoader'
import { IConversion } from './conversion.interfaces'

interface ICreateConversionArgs {
  amount: number
  baseCurrency: string
  targetCurrency: string
  convertedAmount: number
}

interface IPopularCurrency {
  _id: string
  count: number
}

export default class ConversionDataLoader extends SuperDataLoader<IConversion> {
  static async getInstance (): Promise<ConversionDataLoader> {
    const collection = await getCollection('conversion')
    return new ConversionDataLoader(collection, await createLoader(collection))
  }

  async create (input: ICreateConversionArgs): Promise<IConversion> {
    return await this.createDefault({
      input,
    })
  }

  async getMostPopularCurrency (): Promise<string | null> {
    const currenciesSum: Array<IPopularCurrency> = await this.getCollection().aggregate([
      { $group: { _id: '$targetCurrency', count: { $sum: 1 } } },
      { $sort: { count: -1 } },
    ]).toArray()

    return currenciesSum?.[0]?._id
  }
}
