import DataLoader from 'dataloader'
import { Collection, Db, FilterQuery, MongoClient, ObjectId } from 'mongodb'

import { cleanUndefined } from './general'
import set = Reflect.set

export interface IDocumentPage<T> {
  edges: T[]
  total: number
}

export interface IDocument {
  id: string
  createdBy: string
  createdAt: number
  updatedAt: number
  archived: boolean
}

export interface IDataLoader<T> {
  getCollection: () => Collection<T>
  clearCache: () => void
  createDefault: (args: ICreateDefaultArgs<T>) => Promise<T>
  updateOne: (args: IUpdateOneArgs<T>) => Promise<T | null>
  deleteOne: (args: IDeleteOneArgs<T>) => Promise<T | null>
  getById: (args: IGetByIdArgs) => Promise<T | null>
  getOne: (args: IGetOneArgs<T>) => Promise<T | null>
  getByIds: (args: IGetByIdsArgs) => Promise<T[]>
  getPage: (args: IGetAllArgs<T>) => Promise<IDocumentPage<T>>
}

interface ICreateDefaultArgs<T> {
  input: Partial<T>
  createdBy?: string
}

interface IUpdateOneArgs<T> {
  id?: string
  filter?: FilterQuery<T>
  input: Partial<T>
  updatedBy?: string
  includeArchived?: boolean
}

interface IDeleteOneArgs<T> {
  id: string
  filter?: FilterQuery<T>
  deletedBy?: string
}

interface IHardDeleteOneArgs<T> {
  id?: string
  filter?: FilterQuery<T>
}

interface IGetByIdArgs {
  id: string
  includeArchived?: boolean
}

interface IGetOneArgs<T> {
  filter?: FilterQuery<T>
  includeArchived?: boolean
}

interface IGetManyArgs<T> {
  filter?: FilterQuery<T>
  includeArchived?: boolean
}

interface IGetByIdsArgs {
  ids: string[]
  includeArchived?: boolean
}

export interface IPaginationInput {
  pageNumber: number
  pageSize: number
}

interface IGetAllArgs<T> {
  pagination: IPaginationInput
  filter?: FilterQuery<T>
  includeArchived?: boolean
}

interface ICountArgs<T> {
  filter?: FilterQuery<T>
  includeArchived?: boolean
}

let DB_URL: string = process.env.DB_URL as string

let _client: MongoClient
let _db: Db

export default class SuperDataLoader<T extends IDocument> implements IDataLoader<T> {
  private readonly collection: Collection
  private readonly loader: DataLoader<string, T, string>

  constructor (collection: Collection, loader: DataLoader<string, T>) {
    this.collection = collection
    this.loader = loader
  }

  getCollection (): Collection {
    return this.collection
  }

  clearCache (): void {
    this.loader.clearAll()
  }

  async createDefault ({ input, createdBy }: ICreateDefaultArgs<T>): Promise<T> {
    const optional: { createdBy?: string, lastUpdateBy?: string } = {}
    if (createdBy) {
      optional.createdBy = createdBy
      optional.lastUpdateBy = createdBy
    }
    const now = Date.now()
    const result = (
      await this.collection.insertOne({
        ...cleanUndefined(input),
        archived: false,
        createdAt: now,
        updatedAt: now,
        ...optional,
      })
    ).ops[0]

    return normalized<T>(result)
  }

  async updateOne (
    {
      id,
      filter = {},
      input,
      updatedBy,
      includeArchived = false,
    }: IUpdateOneArgs<T>,
  ): Promise<T | null> {
    if (id) {
      set(filter, '_id', new ObjectId(id))
    }
    if (!includeArchived) {
      set(filter, 'archived', false)
    }
    const metadata: { updatedAt: number, lastUpdateBy?: string } = {
      updatedAt: Date.now(),
    }
    if (updatedBy) {
      metadata.lastUpdateBy = updatedBy
    }
    const result = await this.collection.findOneAndUpdate(
      filter,
      {
        $set: {
          ...cleanUndefined(input),
          ...metadata,
        },
      },
      { returnOriginal: false },
    )
    return normalized<T>(result.value)
  }

  async deleteOne ({ id, filter = {}, deletedBy }: IDeleteOneArgs<T>): Promise<T | null> {
    if (id) {
      set(filter, '_id', new ObjectId(id))
    }
    const optional: any = {}
    if (deletedBy) {
      optional.lastUpdateBy = deletedBy
    }
    const result = await this.collection.findOneAndUpdate(
      filter,
      {
        $set: {
          archived: true,
          updatedAt: Date.now(),
          ...optional,
        },
      },
      { returnOriginal: false },
    )
    return normalized<T>(result.value)
  }

  async hardDeleteOne ({ id, filter = {} }: IHardDeleteOneArgs<T>): Promise<T | null> {
    if (id) {
      set(filter, '_id', new ObjectId(id))
    }
    const result = await this.collection.findOneAndDelete(filter)

    return normalized<T>(result.value)
  }

  async getById ({ id, includeArchived = false }: IGetByIdArgs): Promise<T | null> {
    const result = await this.loader.load(id).catch(() => {
      return null
    })
    if (!result) {
      return null
    }
    if (!includeArchived && result.archived) {
      return null
    }

    return normalized<T>(result)
  }

  async getOne ({ filter = {}, includeArchived = false }: IGetOneArgs<T>): Promise<T | null> {
    if (filter.id) {
      set(filter, '_id', new ObjectId(filter.id as string))
      delete filter.id
    }
    if (!includeArchived) {
      set(filter, 'archived', false)
    }
    const result: any = await this.collection.findOne(filter)
    if (result) {
      this.loader.prime(result._id, result)
    }

    return normalized(result)
  }

  async getMany ({ filter = {}, includeArchived = false }: IGetManyArgs<T>): Promise<T[]> {
    if (!includeArchived) {
      set(filter, 'archived', false)
    }
    const results = await this.collection.aggregate([{ $match: filter }]).toArray()
    results.forEach((d) => this.loader.prime(d._id, d))

    return results.map((d) => normalized(d))
  }

  async getByIds ({ ids, includeArchived = false }: IGetByIdsArgs): Promise<T[]> {
    let results: Array<T | Error> = await this.loader.loadMany(ids)
    if (!includeArchived) {
      results = results.filter((r) => !(r instanceof Error) && !r.archived)
    }

    return results.map((d) => normalized(d))
  }

  async getPage (
    {
      pagination: { pageSize, pageNumber },
      filter = {},
      includeArchived = false,
    }: IGetAllArgs<T>,
  ): Promise<IDocumentPage<T>> {
    if (!includeArchived) {
      set(filter, 'archived', false)
    }
    const [
      {
        total: [total = 0],
        edges,
      },
    ]: any = await this.collection
      .aggregate([
        { $match: filter },
        {
          $facet: {
            total: [{ $group: { _id: null, count: { $sum: 1 } } }],
            edges: [{ $skip: pageSize * (pageNumber - 1) }, { $limit: pageSize }],
          },
        },
        {
          $project: {
            total: '$total.count',
            edges: '$edges',
          },
        },
      ])
      .toArray()
    return { total, edges: edges.map((d: any) => normalized(d)) }
  }

  async count ({ filter = {}, includeArchived = false }: ICountArgs<T>): Promise<number> {
    if (!includeArchived) {
      set(filter, 'archived', false)
    }
    return await this.collection.countDocuments(filter, {})
  }
}

async function getClient (): Promise<MongoClient> {
  if (!_client) {
    _client = await MongoClient.connect(DB_URL, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    })
  }

  return _client
}

async function getDb (): Promise<Db> {
  if (!_db) {
    const client = await getClient()
    _db = client.db()
  }
  return _db
}

export function setDbUrl (url: string): void {
  DB_URL = url
}

export async function getCollection (name: string): Promise<Collection> {
  return (await getDb()).collection(name)
}

async function batch<T> (model: Collection, keys: readonly string[]): Promise<T[]> {
  const ids: ObjectId[] = keys.map((key) => new ObjectId(key))
  return await model.find({ _id: { $in: ids } }).toArray()
}

// eslint-disable-next-line @typescript-eslint/require-await
export async function createLoader<T> (collection: Collection): Promise<DataLoader<string, T>> {
  return new DataLoader(async (keys) => await batch(collection, keys), {
    cacheKeyFn: (key: string) => key.toString(),
  })
}

export async function closeConnection (): Promise<void> {
  _client && (await _client.close())
}

export function normalized<T> (document: Record<string, any> | null): T {
  if (document?._id) {
    document.id = document._id.toHexString()
    delete document._id
  }
  return document as T
}
